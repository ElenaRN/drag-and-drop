import React, { useState, useRef } from "react";

//draggable es para poder arrastrar el contenedor
export default function DragNDrop({ data }) {
  const [list, setList] = useState(data);
  const [dragging, setDragging] = useState(false);

  //useRef devuelve un objeto ref mutable cuya propiedad .current se inicializa con el argumento pasado.
  //El objeto devuelto se mantendrá persistente durante la vida completa del componente.
  const dragItem = useRef(); //esta costante contiene los paramentros a los que se dirige la target.
  const dragNode = useRef();//esta constante contiene la target.

  const handleDragStart = (ev, params) => {
    console.log("drag starting", params);
    dragItem.current = params;
    dragNode.current= ev.target;
    dragNode.current.addEventListener('dragend', handleDragEnd)
    setTimeout(() => {
      setDragging(true); //Clousure setTimeOut para que cuando estemos dragging
      // se siga viendo el mismo resultado de background y color que tiene y no un background y color black.
    }, 0);
    
  };

  const handleDragEnter =(ev, params) => {
    console.log('entering drag', params)//te da el index de la posición donde haces dragging con la tarjeta metiendo los params.
    const currentItem = dragItem.current;

    if(ev.target!==dragNode.current) {
      console.log('target is not the same')
      //oldList es basicamente el list de mi useState
      setList(oldList =>{
        //para copiar nuestra objeto y modificarlo
        let newList = JSON.parse(JSON.stringify(oldList));
        //reemplazar los elementos del objeto por posicion de grupo e item.
        newList[params.grpIndex].items.splice(params.itemIndex, 0, newList[currentItem.grpIndex].items.splice(currentItem.itemIndex, 1)[0])
        dragItem.current = params;
        return newList;
      })
    }
  }

  const handleDragEnd = () => {
    console.log ('ending drag')
    setDragging(false)
    dragNode.current.removeEventListener('dragend', handleDragEnd);
    dragItem.current = null;
    dragNode.current = null;
  }

  const getStyles = (params) => {
    const currentItem = dragItem.current;
    if(currentItem.grpIndex ===params.grpIndex && currentItem.itemIndex === params.itemIndex){
      return 'current dnd-item'
    }
    return 'dnd-item'
  }

  return (
    <div className="drag-n-drop">
      {list.map((grp, grpIndex) => (
        <div 
          key={grp.title} 
          className="dnd-group"
          onDragEnter = {dragging && !grp.items.length?(ev) => handleDragEnter(ev, {grpIndex, itemIndex: 0}): null}
        >
          <div className="group-title">{grp.title}</div>
          {grp.items.map((item, itemIndex) => (
            <div
              draggable
              onDragStart={(ev) => {handleDragStart(ev, { grpIndex, itemIndex });}}
              onDragEnter= {dragging? (ev) =>{handleDragEnter(ev, {grpIndex, itemIndex})} :null}
              key={item}
              className={dragging? getStyles({grpIndex, itemIndex}):"dnd-item"}
            >
              {item}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}
