import React from "react";
import "./App.scss";

import DragNDrop from './components/DragNDrop';

const data = [
  {title: 'Pendientes', items: ['Compras navideñas', 'Organizar cena', 'Emails']},
  {title: 'Completadas', items: ['Hacer la comida', 'Quedar con Menchu']}
]

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <DragNDrop data={data}/>
      </header>
    </div>
  );
}

export default App;
